document.write("<script type='text/javascript' src='js/ajax/ajaxToComments.js'></script>")
//var urlImagenes="imagenes/imagenesFormulario"; //para la direccion de carpeta donde estan las imagenes

function llamarAjax()
{      
    var email = document.form.correo.value;
    var name=document.form.nombre.value;
    var mensaje=document.form.mensaje.value;
    var id_noticia=document.form.id_noticia.value;
    var info="correo="+email +"&name="+ name+"&mensaje=" + mensaje+"&id_noticia=" + id_noticia;
    console.log("info before to send: "+info);
    
    $(function() { 
        $("#capaEncimaformulario").animate(                
            {
                width: "370px",            
                height: "450px",
                color: "",
                opacity: 0.4,            
                fontSize: "3em",
                borderWidth: "5px"
            },300,function(){
                $("#capaEncimaformulario").html( '<img src="'+urlImagenes+
                                                '/espera.gif" width="20"'+
                                                'height="20"><br>Enviando Mensaje ...');
            }           
        );

        $.ajax({
          type: "GET",
          url: "js/ajax/procesarRecienEnviados.php",          
          data: info,          // data sent in url
          success: function(dataRpta)          //on recieve of reply with tha data receive of server
          {          
            if(dataRpta)
            {
                console.log("dataRpta:"+dataRpta);
                $("#capaEncimaformulario").html( '<p>Enviando Mensaje ...</p>');
            }
          }
        }).done(function( msg ) {
            console.log( "Data guardada: " + msg );
                                        
            $("#capaEncimaformulario").animate(
            {//para que se esconda el aviso 
                width: "0px",            
                height: "0px",
                color: "",
                //opacity: 0.4,            
                fontSize: "0em",
                borderWidth: "0px",            
            },2000              
            );// para que se pongan los campos vacios a la horaque desaparesca el div de aviso

            var formuBox=document.form;
            formuBox.correo.value="";
            formuBox.correo2.value="";
            formuBox.nombre.value="";                    
            formuBox.mensaje.value="";
            formuBox.send.disabled=true;                                
            
            document.getElementById('capaEncimaformulario').innerHTML = '';           
            document.getElementById('cajaAvisoN').innerHTML = '';// y que se ponga vacio 
            document.getElementById('cajaAvisoC1').innerHTML = '';
            document.getElementById('cajaAvisoC2').innerHTML = '';
            
            llamarAjaxParaComents(id_noticia , 1, 7);
        });

    });         
}

/****************************funcion para comprobar campos vacios*********************************/
var vel,divIma,divRpta,divApa;
function algunTextVacio(vel,divIma,divRpta,divApa)
{
    divNombre=document.getElementById(vel);
    if (divNombre.value != 0){ //alert("I" + divNombre.value + "I");//hecho por (josue)
        if(divNombre.value != "" )
        {
            document.getElementById(divIma).innerHTML = 
            '<img src="'+urlImagenes+'/advertencia/ok_validation.png" width="20" height="20">';
        
            $("#"+divApa+"").fadeOut(250);//parte de desaparecer avisoEnLetras
            
            document.getElementById(divRpta).innerHTML = '';
            return 1;
        }
        else
        {
            $("#"+divApa+"").fadeIn(250);//parte de aparecer avisoLetras
            
            document.getElementById(divIma).innerHTML = 
            '<img src="'+urlImagenes+'/advertencia/erreur_X.png" width="20" height="20">';
            var nam=divNombre.name;
            document.getElementById(divRpta).innerHTML = '- Escribe tu '+nam+'.';
            return 0;
            
        }
    }
}
/****************************funcion para comprobar campos vacios*********************************/

/******************************funcion para comprobar nombre y apellidos**************************/
var nomm;
function nombreApellido(nomm,divIma,divRpta,divApa)
{
    var analizar=document.getElementById(nomm);
    var expresion=/^[a-zA-Z ]{2,30}$/;
    if(analizar.value.match(expresion))
    {
         document.getElementById(divIma).innerHTML = 
        '<img src="'+urlImagenes+'/advertencia/ok_validation.png" width="20" height="20">';
    
        $("#"+divApa+"").fadeOut(250);//parte de desaparecer avisoEnLetras
        
        document.getElementById(divRpta).innerHTML = '';
        return 1;
    }
    else
    {
        $("#"+divApa+"").fadeIn(250);//parte de aparecer avisoLetras
        
        document.getElementById(divIma).innerHTML = 
        '<img src="'+urlImagenes+'/advertencia/erreur_X.png" width="20" height="20">';
        
        document.getElementById(divRpta).innerHTML = '-Escribe bien TU nombre y apellido.';
        return 0;
    }
}

/******************************funcion para comprobar nombre y apellidos**************************/

/*****************************comprobar si es Gmail.com**********************************/
function comprobarSiEsGmail()
{
    var emaill=document.getElementById('correoId2').value;
    var partesEmail=emaill.split("@");
    
    if(partesEmail[1]=="gmail.com")
    {
        $("#checkboxTotal").fadeIn(500);
        return 1;
    }
    else
    {
        $("#checkboxTotal").fadeOut(500);  
        return 0;
    }
}
/*****************************comprobar si es Gmail.com**********************************/

/*****************************comprobar si correo es real con ajax*********************************/
function comprobarEmailReal()
{
    
    var email = "correo="+(document.form.correo.value);

    $(function() { 
        $.ajax({
          type: "GET",
          url: "js/ajax/comprobarCorreo.php",          
          data: email,          // data sent in url
          success: function(dataRpta)          //on recieve of reply with tha data receive of server
          {          
            //if(dataRpta)
            console.log("dataRpta:"+dataRpta);
          }
        }).done(function( msg ) {
          console.log( "Data Saved: " + msg );           
        });

    });         
    
}
/*****************************comprobar si correo es real*********************************/

/*****************************ver si los correos son iguales******************************/
function correosIguales()
{   
    
    if(document.form.correo.value == document.form.correo2.value)
    {
        document.getElementById('cajaAvisoC2').innerHTML = 
        '<img src="'+urlImagenes+'/advertencia/ok_validation.png" width="20" height="20">';
        $("#advertenciaCorreo2").fadeOut(250);//parte de desaparecer avisoEnLetras

        document.getElementById('cajaAdvCorreo2').innerHTML = '';           
        return 1;
    }
    else
    {

        $("#advertenciaCorreo2").fadeIn(250);//parte de aparecer avisoLetras

        document.getElementById('cajaAvisoC2').innerHTML = 
        '<img src="'+urlImagenes+'/advertencia/erreur_X.png" width="20" height="20">';

        document.getElementById('cajaAdvCorreo2').innerHTML = '-Los correos NO son iguales.';   
        return 0;
    }
    
}
/*****************************ver si los correos son iguales******************************/

/*****************************ver si hay mensaje*****************************************/
var rptaMensaje=0,rptaNombre=0,rptaCorreo=0,rptaCorreo2=0;

function verSiHayMensaje()
{
    var mensaje=document.form.mensaje.value;
    var men=mensaje.replace(/ /g,"");
    //alert(men.length);
    //if (mensaje != 0){ // Falta verificar esta parte (JOSUE)
        if(men.length<=20)
        {
            $("#advertenciaMensaje").fadeIn(250);//parte de aparecer avisoLetras

            document.getElementById('cajaAdvMensaje').innerHTML = 'Mensaje muy corto (Min 20 caracteres)';  
            rptaMensaje=0;
            return 0;
          
        }
        else
        {
            $("#advertenciaMensaje").fadeOut(250);//parte de desaparecer avisoEnLetras
            
            document.getElementById('cajaAdvMensaje').innerHTML = '';
            //contarVerdaderos++;
            rptaMensaje=1;
            return 1;
        }
    //}
}
/*****************************ver si hay mensaje*****************************************/

/*****************************funcion unica nombre************************************************/
function unicaNombre(vel,divIma,divRpta,divApa)
{
    var si=algunTextVacio(vel,divIma,divRpta,divApa);
    
    if(si==1)
    {
        rptaNombre=nombreApellido(vel,divIma,divRpta,divApa);        
    }
    else
        rptaNombre= 0;
    
}
/*****************************funcion unica nombre************************************************/

/*****************************funcion unica correo 1************************************************/
function unicaCorreo1(vel,divIma,divRpta,divApa)
{
    var si= algunTextVacio(vel,divIma,divRpta,divApa);
    if(si==1)
    {
        rptaCorreo=comprobarEmailReal();
    }
    else    
        rptaCorreo= 0;                                          
}
/*****************************funcion unica correo 1************************************************/

/*****************************funcion unica correo 2************************************************/
function unicaCorreo2(vel,divIma,divRpta,divApa)
{
    var si= algunTextVacio(vel,divIma,divRpta,divApa);
    if(si==1)
    {
        var si2=correosIguales();
        if(si2==1) 
        {
           rptaCorreo2=comprobarSiEsGmail();
        }
    }
    else
        rptaCorreo2=0;
}
/*****************************funcion unica correo 2************************************************/

/*****************************activar enviar*********************************************************/
function activarEnviar()
{
    var enviar=document.form.send;
    //alert((rptaMensaje +rptaNombre+rptaCorreo+rptaCorreo2));
    if((rptaMensaje +rptaNombre+rptaCorreo+rptaCorreo2)==3)
    {
      //  alert("entroAqui!!");
        
        enviar.disabled=false;
    }    
    else
    {
        //alert("entroAqui!!");
        enviar.disabled=false;
    }
}
/*****************************activar enviar*********************************************************/