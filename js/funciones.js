var contra
function verificarClave() // verify the pass
{    
    var formu=document.form;    
    formu.passJava.value=calcMD5(formu.pass.value);    
}
function validar_logueo() // log in
{   
    var formu=document.form;

    if (formu.correoE.value==0)
    {
        alert("Ingrese su correo");
        formu.correoE.value="";
        formu.correoE.focus();
        return false;
    }
    if (formu.pass.value==0)
    {
        alert("Ingrese su Password");
        formu.pass.value="";
        //form.pass.focus();
        return false;
    }

    formu.pass.value=calcMD5(formu.pass.value);
    formu.submit();	
}
/***********************************(F)login.php/inicio********************************************/

/****************************************(S)formulario Noticias dentro de login*************************************************************/
function estadoInicial() // state initial from create video
{
    var formu=document.formDatosPost;
    formu.checkCombo.checked=true;	
    formu.checkCrear.checked=false;
    formu.categoria.disabled=true;    
    formu.titulo.focus();
    
}	
	
var fromCome;        
function activarDesactivar(fromCome) // active and disable the part of choose // (create Video)
{
    formu=document.formDatosPost;   
    if(fromCome=="checkCrear")
    {    
        if(formu.checkCrear.checked==true)
        {            
            formu.checkCombo.checked=false;
            formu.categoria.disabled=false;
            formu.categorias.disabled=true;        
        } 
        if(formu.checkCrear.checked==false)   
            formu.categoria.disabled=true;
    }
    if(fromCome=="checkCombo")
    {
        if(formu.checkCombo.checked==true)    
        {                          
            formu.categorias.disabled=false;
            formu.checkCrear.checked=false;
            formu.categoria.disabled=true;        
            formu.categoria.value="Crea:";
        }   
        if(formu.checkCombo.checked==false)
            formu.categorias.disabled=true;
    }
}	
	
	
function limpiarCrear()
{
    document.formDatosPost.categoria.value="";    
}

/****************************************(E)formulario Noticias dentro de login*************************************************************/

/*********************************************(S)funciones de imagenes*********************************************/

var nuevoColorDiv="#7EB3CF";
var nuevoColorLetra="#FFF";
var antiguoColorLetra="#069";
var uno,dos;

function cambiarColorOverD(uno,dos)
{
    var div=document.getElementById(uno);
    var divD=document.getElementById(dos);

    div.style.backgroundColor=nuevoColorDiv;
    divD.style.color=nuevoColorLetra;    
}
		
function cambiarColorOutD(uno,dos)
{
    var div=document.getElementById(uno);
    var divD=document.getElementById(dos);

    div.style.backgroundColor="transparent";
    divD.style.color=antiguoColorLetra;   
}	
/*********************************************(E)funciones de imagenes*********************************************/

/**********************************************(S)funciones de Div de Noticias Seleccionables*******************************////

var colorFondo="#aed2d9";
var colorSombra="#74838c";
var sombra=8;
var traslar= "-5px";
var vari;

function cambiarBordeOver(vari)
{
    var div=document.getElementById(vari);
    var divTexto=document.getElementById(vari);

    //cambiar de color a div y sombreado
    div.style.backgroundColor=colorFondo;
    div.style.webkitBoxShadow=sombra + "px " + sombra + "px " + sombra + "px"+ colorSombra;
    div.style.MozBoxShadow=sombra + "px " + sombra + "px " + sombra + "px"+ colorSombra;
    div.style.boxShadow=sombra + "px " + sombra + "px " + sombra + "px"+ colorSombra;

    //mover el div, hacia afuera
    div.style.MozTransform="scale(1) rotate(0deg) translate(" + traslar + ", " + traslar + ") skew(0deg, 0deg)";
    div.style.webkitTransform="scale(1) rotate(0deg) translate(" + traslar + ", " + traslar + ") skew(0deg, 0deg)";
    div.style.oTransform="scale(1) rotate(0deg) translate(" + traslar + ", " + traslar + ") skew(0deg, 0deg)";
    div.style.msTransform="scale(1) rotate(0deg) translate(" + traslar + ", " + traslar + ") skew(0deg, 0deg)";
    div.style.transform="scale(1) rotate(0deg) translate(" + traslar + ", " + traslar + ") skew(0deg, 0deg)";

    //cambiar el color de Letra del div
    divTexto.style.color="#000000";
    
}
	
	
function cambiarBordeOut(vari)
{
    var div=document.getElementById(vari);
    var divTexto=document.getElementById(vari);

    //volver a estado normal el div en color y sombreado
    div.style.backgroundColor="transparent";
    div.style.webkitBoxShadow="0px 0px 0px transparent";
    div.style.MozBoxShadow="0px 0px 0px transparent";
    div.style.boxShadow="0px 0px 0px transparent";

    //volver a estado normal en posicion y letra original
    div.style.MozTransform="scale(1) rotate(0deg) translate(0px, 0px) skew(0deg, 0deg)";
    div.style.webkitTransform="scale(1) rotate(0deg) translate(0px, 0px) skew(0deg, 0deg)";
    div.style.oTransform="scale(1) rotate(0deg) translate(0px, 0px) skew(0deg, 0deg)";
    div.style.msTransform="scale(1) rotate(0deg) translate(0px, 0px) skew(0deg, 0deg)";
    div.style.transform="scale(1) rotate(0deg) translate(0px,0px) skew(0deg, 0deg)";

    divTexto.style.color="#6767B4";
}	
/**********************************************(E)funciones de Div de Noticias Seleccionables*******************************////

/****************************************(S)abrir noticia******************************************************/
var en;
function entrarAnoticia(en,no)
{
    //alert('Los datos ingresados no existen en la base de datos');
    window.location="desarrolloNoticia.php?noti="+en+"&no="+no+"";
}
/****************************************(E)abrir noticia******************************************************/


/***********************(S)PARTE DE COMENTARIOS DE CADA VIDEO*****************************/
/**************************************************************************************/
/**************************************************************************************/

var nomComen;
var colorAcambiar="whitesmoke";
var colorBase="transparent";

/**********(S)esta funcion hace q cada comentario seleccion y cambie cuando el mouse pase por encima **********/
function seleccionCadaComen(nomComen)
{
    //alert(nomComen);
    var div=document.getElementById(nomComen);                  
    div.style.background=colorAcambiar;
}

function deseleccionCadaComen(nomComen)
{
    //alert(colorVerda);
    var div=document.getElementById(nomComen);                  
    div.style.background=colorBase;//cambiando a color base en la funcion "cargarValorIni()"s
}
/**********(E)esta funcion hace q cada comentario seleccion y cambie cuando el mouse pase por encima **********/

/**************************(I)controlador****************************************************/
var colorAcambiarControl="969AFF";
var colorBaseControl="#DCDCDC";
var colorAcambiarBorde="black";
var borderBase="#999";
var letraBaseColor="A3A3A3"
var letraAcambiarColor="black";
var nomTagContro;

function seleccionCadaComenControl(nomTagContro)
{
    //alert(nomComen);
    var div=document.getElementById(nomTagContro);                  
    div.style.background=colorAcambiarControl;
    div.style.borderColor=colorAcambiarBorde;
    div.style.color=letraAcambiarColor;
    div.style.MozTransform="scale(1) rotate(0deg) translate(" + traslar + ", " + traslar + ") skew(0deg, 0deg)";
    div.style.webkitTransform="scale(1) rotate(0deg) translate(" + traslar + ", " + traslar + ") skew(0deg, 0deg)";
    div.style.oTransform="scale(1) rotate(0deg) translate(" + traslar + ", " + traslar + ") skew(0deg, 0deg)";
    div.style.msTransform="scale(1) rotate(0deg) translate(" + traslar + ", " + traslar + ") skew(0deg, 0deg)";
    div.style.transform="scale(1) rotate(0deg) translate(" + traslar + ", " + traslar + ") skew(0deg, 0deg)";
}

function deseleccionCadaComenControl(nomTagContro)
{
    //alert(colorVerda);
    var div=document.getElementById(nomTagContro);  
    div.style.borderColor=borderBase;
    div.style.color=letraBaseColor;
    div.style.background=colorBaseControl;//cambiando a color base en la funcion "cargarValorIni()"s
    div.style.MozTransform="scale(1) rotate(0deg) translate(0px, 0px) skew(0deg, 0deg)";
    div.style.webkitTransform="scale(1) rotate(0deg) translate(0px, 0px) skew(0deg, 0deg)";
    div.style.oTransform="scale(1) rotate(0deg) translate(0px, 0px) skew(0deg, 0deg)";
    div.style.msTransform="scale(1) rotate(0deg) translate(0px, 0px) skew(0deg, 0deg)";
    div.style.transform="scale(1) rotate(0deg) translate(0px,0px) skew(0deg, 0deg)";
}

function dejarSeleccionadoTag(nomTagContro)
{
    var div=document.getElementById(nomTagContro);
    div.style.borderColor=borderBase;
    div.style.color=letraBaseColor;
    div.style.background=colorBaseControl;
    div.style.MozTransform="scale(1) rotate(0deg) translate(" + traslar + ", " + traslar + ") skew(0deg, 0deg)";
    div.style.webkitTransform="scale(1) rotate(0deg) translate(" + traslar + ", " + traslar + ") skew(0deg, 0deg)";
    div.style.oTransform="scale(1) rotate(0deg) translate(" + traslar + ", " + traslar + ") skew(0deg, 0deg)";
    div.style.msTransform="scale(1) rotate(0deg) translate(" + traslar + ", " + traslar + ") skew(0deg, 0deg)";
    div.style.transform="scale(1) rotate(0deg) translate(" + traslar + ", " + traslar + ") skew(0deg, 0deg)";
}
/**************************(E)controlador****************************************************/

/***********************(E)PARTE DE COMENTARIOS DE CADA VIDEO*****************************/
/**************************************************************************************/
/**************************************************************************************/


/***********************(S)FORMULARIO DE COMENTARIOS*****************************/
/**************************************************************************************/
/**************************************************************************************/
function posicionDivFormulario()
{   
    var divEncima=document.getElementById('capaEncimaformulario');
    var divAbajo=document.getElementById('formulario');
    
    divEncima.style.left=divAbajo.style.left;
    divEncima.style.top=divAbajo.style.top;
    //alert(divEncima.style.left);
    
}
/***********************(E)FORMULARIO DE COMENTARIOS*****************************/
/**************************************************************************************/
/**************************************************************************************/

/**********************************(S)ADMINISTRADOR************************************/
/**************************************************************************************/
/**************************************************************************************/
var valu;
function escogerPagina()
{    
    var div=document.comboAdmi;    
    div.submit();
}
/**********************************(E)ADMINISTRADOR************************************/
/**************************************************************************************/
/**************************************************************************************/

