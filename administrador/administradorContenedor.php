<?
include_once "../class/classBD/classAdministradorDb.php";
include_once "../class/classBD/classCategoriaBD.php";


if (isset($_SESSION["sessionJoed"]))
{    
    $objAdministrador=new AdministradorDb();
    $aBuscar= array('id_administrador' => $_SESSION["sessionJoed"]);// array  para poder buscar la coincidencia con la fucnion.obtenerData()
    $atributosNecesarios=array("nombre"); // aqui se le dice a la clase que solo tenga estos campos para sacar estos datos 
                                          // aunque como solo es una coincidencia en el caso de un solo administrador talvez comvenga
                                          // que sea toda la info recogida, no tan solo en nombre como en este caso.(puede ir comentado)
    $objAdministrador->setAtributos($atributosNecesarios);
    
    $datos=$objAdministrador->obtenerData($aBuscar, "="); // el signo igual es para que sean cincidencias iguales.

    $objDatos=new CategoriaBD();
    $datosBlog=$objDatos->getCategorias();

    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Editando la Pagina</title>
<script language="javascript" type="text/javascript" src="../js/funciones.js"></script>
<script language="javascript" type="text/javascript" src="../js/contenedorCentro.js"></script>

<script src="../js/jquery.js"></script>
<link href="../css/estilos.css" type="text/css" rel="stylesheet" />
<link href="../css/estilosDesarrolloNoticia.css" type="text/css" rel="stylesheet" />
<? include_once "../css/cssConPhp.php"; ?>
</head>

<body onload="estadoInicial(),escogerPagina(<? echo $_GET["queHacerAdmi"]; ?>)">
<center>
	<div id="principal">
		<div id="header">
			<div id="headerIzquierda">
            	<div id="bienvenida">
                	<div id="cajaBienvenida">
                    <h2>Bienvenido <? echo $datos[0]['nombre']; ?></h2>
                    <a href="cerrarSecionAdministrador.php">Cerrar Sesi&oacute;n</a>
                    </div>
                </div>
        	</div>
        	<div id="headerCentro">
            	<div id="contenedorImagen">
            		<img src="../imagenes/sportWorldLogo.png" alt="imagen" width="100"  />
                </div>    
        	</div>
        	<div id="headerDerecha">
        	</div>
    	</div>
        <div id="cuerpo">
        	<div id="parteIzquierda">
            	<div id="separadorEnColum"></div>
            	<div id="tituloPi">
                    <div id="cajaTitulo">
                        <h3>Adminitrador: </h3>
                    </div>
                </div>
                <div id="separadorEnColum"></div>
                
                <div id="choseeAction">
                    <form action="administradorContenedor.php" method="get" name="comboAdmi">
                        <select onchange="escogerPagina()" name="queHacerAdmi">
                            <?
                            if($_GET["queHacerAdmi"]=="")
                            {                             
                            ?>
                            <option value="escoje" >--- Escoje una opcion: ---</option>       
                            <option value="editar" >Editar Existente: </option>
                            <option value="crear" >Crear Nuevo: </option>
                            <?
                            }
                            if($_GET["queHacerAdmi"]=="editar")
                            {                             
                            ?>                        
                            <option value="editar" selected="selected" >Editar Existente</option>
                            <option value="crear" >Crear Nuevo</option>   
                            <?
                            }
                            if($_GET["queHacerAdmi"]=="crear")
                            {
                            ?>
                            <option value="editar" >Editar Existente</option>
                            <option value="crear" selected="selected" >Crear Nuevo</option> 
                            <?
                            }                        
                            ?>
                        </select>
                    </form>
                </div>
                <div id="answer">
                    <?
                    if($_POST["titulo"]) //value from form "create" so this is to know if exits "crear" and their values
                    {
                        $objEnviarDatos=new Web();
                        $value=$objEnviarDatos->ingresarDatosPagina();
                    
                    echo "<div id='answerFromBD'>";
                        if($value)                            
                            echo "Los datos fueron enviados";
                        else
                            echo "los datos no fueron enviados, vuelva a intentarlo";
                    echo "</div>";        

                    }

                    if(isset($_GET["queHacerAdmi"]))
                    {
                       if($_GET["queHacerAdmi"]=="editar")
                           include_once "editarExistente.php"; // para la parte del menu centro 

                       if($_GET["queHacerAdmi"]=="crear")                                                                   
                            include_once "crearNuevoTema.php"; // para la parte del menu centro                    
                    }                

                    ?>
                </div>
                <div id="separadorEnColum"></div>
            </div>
            
            <? include_once "../indexContenido/ContenedorBodyCentro.php"; // para la parte del menu centro ?>
            
            <div id="parteDerecha">
            	<div id="tituloDere">
                	<div id="cajaTituloDere">
                    <h3>Mis movientos en la web</h3>
                    </div>
                </div>
            	<div id="movimientosWeb">
                	
                </div>
            </div>
        </div>
        <div id="lastHeader">
        <hr>
			&copy; Desarrollado por Eduardo Chauca - <? echo date("Y");?>
        </hr>    
        </div>
	</div>    
</center>    
</body>
</html>
<?
}
else{
	echo "
	<script type='text/javascript'>
	alert('Debe loguearse primero para acceder a este contenido');
	window.location='login.php';
	</script>
	";
}
?>