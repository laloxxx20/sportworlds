<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//include_once '../classModelBD/classNoticiaBD.php';

class FirstScreen 
{
    private $posts=array();
    
    public function __construct($cat,$whereIs) 
    {
        $news=new NoticiaBD();
        if($cat==0)
        {
            $generalOno=1;    
            $cosaAbuscar="0";
            $ordenarPorCampo="gusta";
        }
        else
        {
            $generalOno=0;
            $cosaAbuscar=array("id_categoria" => $cat);
            $ordenarPorCampo="id_noticia";
        }
        $sign="=";            
        $conLimit=1; 
        $tamanyoPetici=8; 
        $dondeSeQuedo=$whereIs; 
        $ascOdes="desc";
        $this->posts=$news->obtenerDataPorPartes($generalOno, $cosaAbuscar, $sign, $ordenarPorCampo,
                                                 $conLimit, $tamanyoPetici, $dondeSeQuedo, $ascOdes);
    }
    
    public function getPosts()
    {
        return $this->posts;
    }
}

?>
