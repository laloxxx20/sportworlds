<?php
include_once 'classAdministrador.php';
include_once 'classVideo.php';
include_once 'classMenu.php';
include_once 'classFirstScreen.php';
include_once 'classComments.php';

class System
{
    private $administrador;
    private $video;
    private $dataVideo=array();  
    private $menu; //object menu
    private $categoriess=array();//list with cateries
    private $posts=array(); // list of post
    private $firstScreen;


    public function showDataVideo($cat,$numVid)
    {
        $this->video=new Video($cat, $numVid);         
        $cate=array('categoria' => $this->video->getCategory() );        
        $idCate=array('id_categoria' => $this->video->getIdCategory() );     
        $title=array('title' => $this->video->getTitle());
        $author=array('author' => $this->video->getAuthor());
        $date=array('date' => $this->video->getDate());
        $descript=array('description' => $this->video->getDescription());
        $vid=array('videoUrl' => $this->video->getVideoUrl());
        $this->dataVideo=array_merge((array)$cate,(array)$idCate,(array)$title,(array)$author,
                                     (array)$date,(array)$descript,(array)$vid);
                
        return $this->dataVideo;        
    }
    
    public function showCategorys()
    {
        $this->menu=new Menu();
        $this->categoriess=$this->menu->getCategoriesMenu();
        return $this->categoriess;
    }
    
    public function showPosts($cat,$whereIs)
    {
        $this->firstScreen=new FirstScreen($cat,$whereIs);
        $this->posts=$this->firstScreen->getPosts();
        
        return $this->posts;
    }
    
}

?>
