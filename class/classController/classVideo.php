<?php
/**
 * Description of classVideo
 *  this class means the instance of video or video blog 
 * author Administrador
 */

//include_once "../classModelBD/classCategoriaBD.php";// yet useable ... changing to MVC with de newt link
//include_once "clasComments.php";

class Video 
{    
    private $idCategory;
    private $category;
    private $intoCategory; // can be: something the topic into the category!
    private $titleVideo;
    private $author;
    private $description;
    private $comments;
    private $date;
    private $videoUrl;
  
    public function __construct($cate, $numTuto) 
    {
        /*********************(S)give data with CategoriaBD***************/
        $objNombreTabla=new CategoriaBD(); // objeto para sacar informacion del la categoria seleccionada en si para el nombre y id
        $varParaNombreTabla = array('id_categoria' => $cate);
        $ver=$objNombreTabla->obtenerData($varParaNombreTabla,"="); // function from parent(web)
        /*********************(E)give data with CategoriaBD***************/
        $this->category=$ver[0]['categoria'];        
        $this->idCategory=$cate;    
        
        /*************************(S)data of 'Noticia'******************************/        
        $objetoNoticia=new NoticiaBD();                        
        $buscar=array("id_noticia" => $numTuto);// lo que se va a buscar
        $signoDNoti="=";
        $arrayConDataNoti=$objetoNoticia->obtenerData($buscar, $signoDNoti); //array con la data de los campos que pusimos en 
                                                                     //$atributisNoticia solo esos.ahora con este trabajamos.
        $this->titleVideo=$arrayConDataNoti[0]['titulo'];
        $this->author=$arrayConDataNoti[0]['autor'];
        $this->description=$arrayConDataNoti[0]['detalle'];
        $this->date=$arrayConDataNoti[0]['fecha_cadena'];
        $this->videoUrl=$arrayConDataNoti[0]['html'];
        $this->comments=new Comments();
        /*************************(E)data of 'Noticia'******************************/
        
    }
    
    public function getIdCategory() 
    {
        return $this->idCategory;
    }    
    
    public function getCategory() 
    {
        return $this->category;
    }
    
    public function getTitle() 
    {
        return $this->titleVideo;
    }
    
    public function getAuthor()
    {
        return $this->author;
    }
    
    public function getDescription() 
    {
        return $this->description;
    }
    
    public function getDate() 
    {
        return $this->date;
    }
    public function getVideoUrl() 
    {
        return $this->videoUrl;
    }
    
    public function setComments($comment)
    {
        
    }
    /*
    public function setIdCategory($name)
    {
        $this->idCategory=$name;
    }*/
    /*
    public function setCategory($name)
    {
        $this->category=$name;
    }*/
    
    
    
}

?>
