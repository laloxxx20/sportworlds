<?php
include_once "classWeb.php";

class CategoriaBD extends Web
{
    function __construct()
    {        
        parent::$this->atributos=array("id_categoria","categoria");        
        parent::$this->nombreTabla= "categorias"; // con esta data nacera siempre este objeto .. 
                                                  //pero puede ser cambiada por get y set de la        
                                                  // clase padre .Web.        
    }
    
    public function getCategorias()
    {
        $generalOno=1;
        $cosaAbuscar="0";//id auqi ira get opos
        $ordenarPorCampo="id_categoria";
        $conLimit=0;
        $tamanyoPetici="0";
        $dondeSeQuedo="0";
        $ascOdes="asc";
        return parent::obtenerDataPorPartes($generalOno,$cosaAbuscar,"=",$ordenarPorCampo,
                                            $conLimit,$tamanyoPetici,$dondeSeQuedo,$ascOdes);
    }
}

?>
