-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- Servidor: localhost
-- Tiempo de generación: 22-02-2012 a las 23:02:56
-- Versión del servidor: 5.0.51
-- Versión de PHP: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

USE DATABASE sportsworld;

-- 
-- Base de datos: `sportsworld`
-- 

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `administrador`
-- 

CREATE TABLE `administrador` (
  `nombre` varchar(100) character set utf8 collate utf8_spanish_ci NOT NULL,
  `contraPhp` varchar(500) character set utf8 collate utf8_spanish_ci NOT NULL,
  `correo` varchar(50) character set utf8 collate utf8_spanish_ci NOT NULL,
  `id_administrador` int(11) NOT NULL auto_increment,
  `contraJs` varchar(500) character set utf8 collate utf8_spanish_ci NOT NULL,
  `estado` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `user` varchar(50) character set utf8 collate utf8_spanish_ci NOT NULL,
  PRIMARY KEY  (`id_administrador`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- 
-- Volcar la base de datos para la tabla `administrador`
-- 

INSERT INTO `administrador` VALUES ('Javier Chauca Gallegos', '295ac072cd30755d6934cb66fcd07c14', 'lalocura90_6@hotmail.com', 1, '61604D542F45CF07026FADC8EF3E7611', 0, 0, 'laloxxx20');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `categorias`
-- 

CREATE TABLE `categorias` (
  `id_categoria` int(11) NOT NULL auto_increment,
  `categoria` varchar(100) character set utf8 collate utf8_spanish_ci NOT NULL,
  PRIMARY KEY  (`id_categoria`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

-- 
-- Volcar la base de datos para la tabla `categorias`
-- 

INSERT INTO `categorias` VALUES (1, 'futbol');
INSERT INTO `categorias` VALUES (2, 'VoleyBall');
INSERT INTO `categorias` VALUES (3, 'tennis');
INSERT INTO `categorias` VALUES (4, 'futsal');
INSERT INTO `categorias` VALUES (5, 'basketball');
INSERT INTO `categorias` VALUES (6, 'baile');
INSERT INTO `categorias` VALUES (7, 'fisicoculurismo');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `comentarios`
-- 

CREATE TABLE `comentarios` (
  `id_comentario` int(11) NOT NULL auto_increment,
  `nombre` varchar(150) character set utf8 collate utf8_spanish_ci NOT NULL,
  `correo` varchar(100) NOT NULL,
  `web` varchar(100) NOT NULL,
  `texto` text character set utf8 collate utf8_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `id_noticia` int(11) NOT NULL,
  PRIMARY KEY  (`id_comentario`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=104 ;

-- 
-- Volcar la base de datos para la tabla `comentarios`
-- 

INSERT INTO `comentarios` VALUES (29, 'Eduardo Chauca Gallegos', 'lalocura90_6@hotmail.com', 'www.preelec.tk', 'hola esta es una muy buena web .... ajajaja esta es una prueba de coment', '2011-02-23', 26);
INSERT INTO `comentarios` VALUES (30, 'Jorge Pando', 'joed@gmail.com', 'www.jojo.www', 'hola jajajajjaja hola jajajajjaja hola  jajajajjaja hola jajajajjaja hola jajajajjaja hola jajajajjaja hola jajajajjaja hola jajajajjaja hola jajajajjaja', '2011-12-26', 26);
INSERT INTO `comentarios` VALUES (31, 'Diego Ortiz Selaco', 'diego@hotmail.com', 'www.diwgo.com', 'muy buena web veamos como es que se ven todos los comentarios con informacion muy buena web veamos como es que se ven todos los comentarios con informacion', '2011-12-26', 26);
INSERT INTO `comentarios` VALUES (32, 'laloxxx', 'lalloxxx,@gmail.com', 'www.jeje.com', 'es pero  q es estos coments ayujden a la web y que cada vez sea mejor para poder expresarla mejor y mostrar la version beta a patas que queiran colaborar programandola', '2011-12-31', 26);
INSERT INTO `comentarios` VALUES (33, 'Gladys Gallegos Pacheco', 'gladys@hotmail.com', 'www.gladys.com', 'hola je este el nombre de mi mamam e squ eya no se que escribir buen esto sera tdo.', '2011-12-31', 26);
INSERT INTO `comentarios` VALUES (38, 'karan Chauca Gallegos', 'haren@gmail.com', 'www.karen.com', 'hola que  yo tambiene stoy aqui para apoyar con los coments de web ya fatla poco para ver la estrcutra je eeee', '2011-12-31', 26);
INSERT INTO `comentarios` VALUES (39, 'Jorge Garcia Enoki', 'jorge@gmail.com', 'www.jorge.com', 'hola je espero se ryo ta mbien uno de los constructores de esta web \r\nmas adelante cuadno nuestro pata eduardo termine la version beta y nos convenza con esta.', '2011-12-31', 26);
INSERT INTO `comentarios` VALUES (37, 'Yayo chauca Cruces', 'yayozxxx@gmail.com', 'www.preelec.tk', 'hola je espero que sea rapido lo que escrbire pues solo son purebas para apoyar a ver la estrcutura de la web', '2011-12-31', 26);
INSERT INTO `comentarios` VALUES (40, 'jeff  Aguirre Corrales', 'jeff@gmail.com', 'www.jeff.com', 'hola espero que la wb se vea vea bien con estos ultimos oments de prueba je \r\ny que no se vea mal, espero que el color mejore y cosas por ahy que faltan,\r\nxvr', '2011-12-31', 26);
INSERT INTO `comentarios` VALUES (41, 'lalo', 'lalo@hotmail.com', '', 'hola espero que esta web sea un exito y veamos si estos comentarios se logran posicionar como quiero en la primera prueba.                                        ', '2012-02-03', 26);
INSERT INTO `comentarios` VALUES (42, 'ed', 'eduardo@hotmail.com', '', 'eduardo@hotmail.comeduardo@hotmail.comeduardo@hotmail.com                                        ', '2012-02-03', 26);
INSERT INTO `comentarios` VALUES (43, 'ed', 'eduardo@hotmail.com', '', 'hola como estan espero  que este coment sea el primero de esta seccion                                        ', '2012-02-03', 27);
INSERT INTO `comentarios` VALUES (51, 'Bensda', 'Bensda@hotmail.com', '', 'hola je como estas epero que este si funcione                                        ', '0000-00-00', 26);
INSERT INTO `comentarios` VALUES (45, 'jorginyo', 'jorginyo@hotmail.com', '', 'jorginyo@hotmail.com hola hajajajaja veamos                                        ', '2012-02-03', 27);
INSERT INTO `comentarios` VALUES (46, 'chullls', 'chullls@hotmail.com', '', 'hola es lneta la circulacuon en la panemericana sur desde el punete atocongo                                        ', '2012-02-03', 26);
INSERT INTO `comentarios` VALUES (47, 'katy Perry', 'katyperry@hotmail.com', '', 'hola jajaja parece que ya salio pero aun sigue haibendo un problema con ajax ... veremos que es                                        ', '2012-02-03', 26);
INSERT INTO `comentarios` VALUES (48, 'studio', 'studio@hotmail.com', '', 'hola hajajaj sigo probando la seccion de los cometasrios                                         ', '2012-02-03', 26);
INSERT INTO `comentarios` VALUES (49, 'Lady', 'Lady@hotmail.com', '', 'tu crees que yo no reconocido .. raul el que t agarro en el baul?                                        ', '2012-02-03', 26);
INSERT INTO `comentarios` VALUES (50, 'jefrey', 'jfre@hotmail.com', '', 'hola ajajaj veamos que pasa ahora prueba 14                                        ', '2012-02-03', 26);
INSERT INTO `comentarios` VALUES (52, 'chino', 'chinox@hotmail.com', '', 'hola you jajajaja hooo jajajaja jejeje uuuuuuuuuuuuuuuuu allllllll                                        ', '0000-00-00', 26);
INSERT INTO `comentarios` VALUES (53, 'infenitive', 'infenitive@hotmail.com', '', 'is follow better who can save the world tonight                                        ', '0000-00-00', 26);
INSERT INTO `comentarios` VALUES (54, 'buraco', 'buraco@hotmail.com', '', 'desafiante incontrolable voy jajajaja                                        ', '0000-00-00', 26);
INSERT INTO `comentarios` VALUES (55, 'redHor', 'redHor@hotmail.com', '', 'veamos cuantos comentarios mas tendre que hacer para poder realizar bien esto                                        ', '0000-00-00', 26);
INSERT INTO `comentarios` VALUES (56, 'lifes', 'lifes@hotmail.com', '', 'veamos que pasara esta ves                                        ', '0000-00-00', 26);
INSERT INTO `comentarios` VALUES (57, 'O no ', 'idontknow@hotmail.com', '', 'no lo se pero esta ves tiene que salir bien                                        ', '0000-00-00', 26);
INSERT INTO `comentarios` VALUES (58, 'mrsaxobeat', 'mrsaxobeat@hotmail.com', '', 'um a ja saxo beat jajaja tu tut ut                                        ', '0000-00-00', 26);
INSERT INTO `comentarios` VALUES (59, 'robert', 'robert@yahoo.com', '', 'es saxo beat , tratemos de rrellenar los campos                                        ', '0000-00-00', 26);
INSERT INTO `comentarios` VALUES (60, 'gian marco', 'gianmarco@htomail.com', '', 'cuentame que ha sido de tu vida , dime cuentame                                        ', '0000-00-00', 26);
INSERT INTO `comentarios` VALUES (61, 'Lmfao', 'Lmfao@hotmail.com', '', 'Lmfao@hotmail.com jajaja ed                                        ', '0000-00-00', 26);
INSERT INTO `comentarios` VALUES (62, 'Aveces', 'Aveces@hotmail.com', '', 'ho u ho u nanan ananananana                                        ', '0000-00-00', 26);
INSERT INTO `comentarios` VALUES (63, 'Broken Bells', 'broken@hotmail.com', '', 'jajaja peligro y otra vez corro por la linea de fuego                                        ', '0000-00-00', 13);
INSERT INTO `comentarios` VALUES (64, 'Amen', 'amen@hotmail.com', '', 'hola jajaja espero que eta vez se vea bien                                        ', '0000-00-00', 26);
INSERT INTO `comentarios` VALUES (65, 'Gian', 'Gian@hotmail.com', '', 'cuantme la vida a cambiardo que sientes si me miras                                        ', '0000-00-00', 26);
INSERT INTO `comentarios` VALUES (66, 'monarqui', 'monarqui@hotmail.com', '', 'monarqui@hotmail.com jajajajajaja                                        ', '0000-00-00', 26);
INSERT INTO `comentarios` VALUES (67, 'heyee', 'heyee@hotmail.com', '', 'heyee@hotmail.com jajasjj s                                        ', '0000-00-00', 26);
INSERT INTO `comentarios` VALUES (68, 'segundo ', 'segundo@hotmail.com', '', '          veamos el segundo comentario de esto je :P                              ', '0000-00-00', 13);
INSERT INTO `comentarios` VALUES (69, 'dady', 'dady@hotmail.com', '', 'dady@hotmail.com like a freak oooo                                        ', '0000-00-00', 26);
INSERT INTO `comentarios` VALUES (70, 'jagger', 'jagger@hotmail.com', '', 'jagger@hotmail.com like jagger                                        ', '0000-00-00', 13);
INSERT INTO `comentarios` VALUES (71, 'marco', 'marco@hotmail.com', '', 'marco jajajajajajajjajaja                                        ', '0000-00-00', 13);
INSERT INTO `comentarios` VALUES (72, 'eduka', 'eduka@hotmail.com', '', 'no buscaba nada caminaba sin pensar per ocuadno vi tu cara no me pude alejar no me lo esperaba y no supe reaccionar no encontraba las palabras ni el momento para hablar y t mira da me corta la respiracion                                        ', '0000-00-00', 13);
INSERT INTO `comentarios` VALUES (73, 'reik', 'reik@hotmail.com', '', 'reik@hotmail.com jajajaja                                        ', '0000-00-00', 13);
INSERT INTO `comentarios` VALUES (74, 'nek', 'nek@hotmail.com', '', 'nek@hotmail.com el heroe no revela la verdad                                        ', '0000-00-00', 13);
INSERT INTO `comentarios` VALUES (75, 'jovan', 'jovan@hotmail.com', '', 'jovan ahahaha sjlkajskljd                                        ', '0000-00-00', 13);
INSERT INTO `comentarios` VALUES (76, 'que CAlor', 'queor@hotmail.com', '', 'queor@hotmail.com ven y tocame ven dominme                                        ', '0000-00-00', 17);
INSERT INTO `comentarios` VALUES (77, 'who', 'whokind@hotmail.com', '', 'whokind@hotmail.com hohohoh                                        ', '0000-00-00', 13);
INSERT INTO `comentarios` VALUES (78, 'ready', 'ready@hotmail.com', '', 'ready show me that resity ooaoa                                        ', '0000-00-00', 13);
INSERT INTO `comentarios` VALUES (79, 'reika', 'reika@hotmail.com', '', 'reika@hotmail.com una y uotr aves                                        ', '0000-00-00', 13);
INSERT INTO `comentarios` VALUES (80, 'peligro', 'peligro@hotmail.com', '', 'peligro@hotmail.com hosao                                        ', '0000-00-00', 13);
INSERT INTO `comentarios` VALUES (81, 'sentido', 'sentido@hotmail.com', '', 'sentido@hotmail.com hoao                                        ', '0000-00-00', 13);
INSERT INTO `comentarios` VALUES (82, 'quick', 'quick@hotmail.com', '', 'quick@hotmail.com jasjdj                                        ', '0000-00-00', 13);
INSERT INTO `comentarios` VALUES (83, 'marDe', 'mardecopas@hotmail.com', '', 'mardecopas@hotmail.com que fuerte suena el viento asi pasa la vida te dejo mi silecnia mellevoa lguna pena deti                                         ', '0000-00-00', 13);
INSERT INTO `comentarios` VALUES (84, 'conmigo', 'conmigo@hotmail.com', '', 'conmigo dejame demostrar que esto no ha sido en vano                                        ', '0000-00-00', 13);
INSERT INTO `comentarios` VALUES (85, 'around', 'around@hotmail.com', '', 'around@hotmail.com jajaja veamos esto                                        ', '0000-00-00', 13);
INSERT INTO `comentarios` VALUES (86, 'filosofi', 'filosofi@hotmail.com', '', 'filosofi@hotmail.com realx take your time                                        ', '0000-00-00', 26);
INSERT INTO `comentarios` VALUES (87, 'infenetive', 'infenetive@hotmail.com', '', 'infenetive@hotmail.com thar you will see                                        ', '0000-00-00', 13);
INSERT INTO `comentarios` VALUES (88, 'jefferson', 'jefferson@hotmail.com', '', 'jefferson@hotmail.com jajaja                                        ', '0000-00-00', 13);
INSERT INTO `comentarios` VALUES (89, 'juanes', 'juanes@hotmail.com', '', 'juanes@hotmail.com y peudo aparenta r qeu esto no me intersa                                        ', '0000-00-00', 13);
INSERT INTO `comentarios` VALUES (90, 'edmundo', 'edmundo@hotmail.com', '', 'edmundo hola como estan esperoq eu bien                                        ', '0000-00-00', 13);
INSERT INTO `comentarios` VALUES (91, 'sebastian', 'sebastian@hotmail.com', '', 'sebastian@hotmail jejejej', '0000-00-00', 13);
INSERT INTO `comentarios` VALUES (92, 'igetwe', 'igetwe@hotmail.com', '', 'igetwe@hotmail.com jajaja no funciona bien', '0000-00-00', 13);
INSERT INTO `comentarios` VALUES (93, 'fadein', 'fadein@hotmail.com', '', 'fadein@hotmail.com jaja                                        ', '0000-00-00', 13);
INSERT INTO `comentarios` VALUES (94, 'veamos', 'veamos@hotmail.com', '', 'veamos@hotmail.com veamos si funciona', '0000-00-00', 13);
INSERT INTO `comentarios` VALUES (95, 'filosofi', 'filosofi@hotmail.com', '', 'filosofi@hotmail.com realx take your time                                        ', '0000-00-00', 26);
INSERT INTO `comentarios` VALUES (96, 'filosofi', 'filosofi@hotmail.com', '', 'filosofi@hotmail.com realx take your time                                        ', '0000-00-00', 26);
INSERT INTO `comentarios` VALUES (97, 'filosofi', 'filosofi@hotmail.com', '', 'filosofi@hotmail.com realx take your time                                        ', '0000-00-00', 26);
INSERT INTO `comentarios` VALUES (98, 'Diego', 'diegoalonzo123@hotmail.com', '', 'hola administradores esta pagina esta muy buena                                        ', '0000-00-00', 15);
INSERT INTO `comentarios` VALUES (99, 'lalocura', 'lalocura@hotmail.com', '', 'hola como estas esto es una prueba para los comentarios de sprotworld jajaj su mpresora se le malogrado a este wey!!!!                                        ', '0000-00-00', 15);
INSERT INTO `comentarios` VALUES (100, 'Gladys', 'Gladys@hotmail.com', '', 'garabaos garabatos aca meirda donde dice galdys mira el efecto de carga de cometnarios                                        ', '0000-00-00', 15);
INSERT INTO `comentarios` VALUES (101, 'Tizianos', 'Tizianos@hotmail.com', '', 'si estoy alucinado yo como estas preguntas  a mi el amarte me vualve predecible hablo poco es extraÃ±o voy muy el fuego                                        ', '0000-00-00', 13);
INSERT INTO `comentarios` VALUES (102, 'ed', 'ed@hotmail.com', '', 'ed@hotmail.com jajja sigue fucnionando?                                        ', '0000-00-00', 18);
INSERT INTO `comentarios` VALUES (103, 'messi', 'messi@hotmail.com', '', 'primer comentario de messi@hotmail.com veamos como va                                         ', '0000-00-00', 28);

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `noticias`
-- 

CREATE TABLE `noticias` (
  `id_noticia` int(11) NOT NULL auto_increment,
  `autor` varchar(150) NOT NULL,
  `titulo` varchar(150) character set utf8 collate utf8_spanish_ci NOT NULL,
  `detalle` text character set utf8 collate utf8_spanish_ci NOT NULL,
  `html` text character set utf8 collate utf8_spanish_ci NOT NULL,
  `fecha_cadena` varchar(100) character set utf8 collate utf8_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `descarga` varchar(100) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `gusta` bigint(20) NOT NULL,
  PRIMARY KEY  (`id_noticia`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

-- 
-- Volcar la base de datos para la tabla `noticias`
-- 

INSERT INTO `noticias` VALUES (1, '', '1 baile', 'holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas\r\n', '', '26 de February 2011', '2011-02-26', '', 6, 4);
INSERT INTO `noticias` VALUES (2, '', '2 tuto', 'holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas\r\n', '', '26 de February 2011', '2011-02-26', '', 6, 0);
INSERT INTO `noticias` VALUES (3, '', '3 tuto', 'holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas\r\n', '', '28 de February 2011', '2011-02-28', '', 6, 7);
INSERT INTO `noticias` VALUES (4, '', '4 tuto', 'holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas\r\n', '', '28 de February 2011', '2011-02-28', '', 6, 2);
INSERT INTO `noticias` VALUES (5, '', '5 tuto', 'holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas\r\n', '', '28 de February 2011', '2011-02-28', '', 6, 0);
INSERT INTO `noticias` VALUES (6, '', '6 tuto', 'holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas\r\n', '', '28 de February 2011', '2011-02-28', '', 6, 0);
INSERT INTO `noticias` VALUES (7, '', '7 tuto', 'holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas\r\n', '', '28 de February 2011', '2011-02-28', '', 6, 0);
INSERT INTO `noticias` VALUES (8, '', '8 tuto', 'holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas holas hoals holas\r\n', '', '28 de February 2011', '2011-02-28', '', 6, 8);
INSERT INTO `noticias` VALUES (9, '', '1 tuto', 'jajajajajajajajajajajjajajajajajajajajajaj\r\njajajajajajajajajajajjajajajajajajajajajaj\r\njajajajajajajajajajajjajajajajajajajajajaj', '', '28 de February 2011', '2011-02-28', '', 5, 3);
INSERT INTO `noticias` VALUES (10, '', '2 tuto', 'jajajajajajajajajajajjajajajajajajajajajaj\r\njajajajajajajajajajaj\r\njajajajajajajajajajajjajajajajajajajajajaj', '', '28 de February 2011', '2011-02-28', '', 5, 0);
INSERT INTO `noticias` VALUES (11, '', '9 tuto', 'joojojojojojojojooooooooooooooojojojojojo\r\njoojojojojojojojooooooooooooooojojojojojo\r\njoojojojojojojojooooooooooooooojojojojojo\r\n', '', '1 de March 2011', '2011-03-01', '', 6, 0);
INSERT INTO `noticias` VALUES (12, '', '10 tuto', 'jaaaaaaaaaa aaaaaaaaaaaa aaaaaaaaaaa aaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaa aaaaaaaa aaaaaaaaa aaaaaaaaaaa aaaaa aaaaaaa aaaaaaaa', '', '1 de March 2011', '2011-03-01', '', 6, 2);
INSERT INTO `noticias` VALUES (13, '', 'segundo video tutotirla', 'melasa', '<iframe src="http://player.vimeo.com/video/12392831" width="380" height="300" frameborder="0"></iframe>', '22 de March 2011', '2011-03-22', '', 7, 1);
INSERT INTO `noticias` VALUES (14, '', 'vaaaaa', 'vaaaaaaaaaaaaaaa aaaaaaaaaaaaaaaaa aaaaaaaaaaaaaaaaa aaaaaaaaaaaaaaaaa aaaaaaaaaa', '', '23 de mayo', '2011-05-16', '', 6, 9);
INSERT INTO `noticias` VALUES (15, '', 'video 2', 'waaaaaaaaaaaa aaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaa aaaaaaaaaaaaaaaaaa', '', '11 de mayo', '2011-05-11', '', 1, 17);
INSERT INTO `noticias` VALUES (16, '', 'viveo 3', 'todo el universo somos tu y yo tu sonrisa de angel se durmio duerme a mi lado te imaginare y louego de soñarte despeertare', '', '24 de mayp', '2011-05-24', '', 1, 21);
INSERT INTO `noticias` VALUES (17, '', 'tuto 2', 'des a million causen baut  so beybi a tha best time ever had  yuo beiby so one in a million you are best time ever had', '', '12 mayo', '2011-05-12', '', 1, 1);
INSERT INTO `noticias` VALUES (18, '', 'tuto 3 ', 'hfgdhffffffffffffffffffffffggggg fg df d      ddddddddddddddddddddd       ddddddd', '', '23 de abril', '2011-05-17', '', 2, 2);
INSERT INTO `noticias` VALUES (19, '', 'tuto 1', 'hfgdhffffffffffffffffffffffggggg fg df d      ddddddddddddddddddddd       ddddddd', '', '12 de mayo', '2011-05-18', '', 2, 4);
INSERT INTO `noticias` VALUES (20, '', 'tuto 16', 'hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola ', '', '13 de mayo', '2011-05-19', '', 6, 0);
INSERT INTO `noticias` VALUES (21, '', 'tuto 17', 'hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola ', '', '12 de amyo', '2011-05-05', '', 6, 14);
INSERT INTO `noticias` VALUES (22, '', 'tuto 18', 'hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola ', '', '43 de abril', '2011-05-26', '', 6, 0);
INSERT INTO `noticias` VALUES (23, '', 'tuto 19', 'hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola ', '', '14 de mayo', '2011-05-18', '', 6, 1);
INSERT INTO `noticias` VALUES (24, '', 'tuto 20', 'hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola ', '', '1 de enero', '2011-05-01', '', 6, 1);
INSERT INTO `noticias` VALUES (25, '', 'tuto 21', 'hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola hola ', '', '13 avbril', '2011-05-19', '', 6, 3);
INSERT INTO `noticias` VALUES (26, 'Eduardo Chauca Cruces', 'tuto 22', 'The deadline for the application is the 15th of September, 2011. Karin Breitman is an assistant professor at PUC-Rio and former Computer Engineering Program coordinator. Her research focuses on Software Engineering and its applications in Knowledge Representation, Requirements Engineering, Semantic Web and Cloud Computing. She is the Publications Director of the Brazilian Computer Society, and IFIP''s TC-1 Secretary. In 2009 she was the PC Chair of two IEEE conferences (ICECCS''09 & SEW''09), 2 workshops (IEEE ECBS-LARC''09 and WIT), as well as FME''s sponsored ICFEM''09. Author of over 70 research papers in conference proceedings and journals, she received numerous research grants from public institutions and private industry, including NASA, CNRS, IBM and, more recently, Hewlett Packard. Her new books, "Analogy and Metaphor in Information Technology" - Springer Verlag and Cloud', '<iframe width="380" height="300" src="http://www.youtube.com/embed/0gpw4qHFoZY" frameborder="0" allowfullscreen></iframe>', '13 noviuembre', '2011-05-06', '', 6, 3);
INSERT INTO `noticias` VALUES (27, '', 'Preparacion Fisica 1', 'Primera clase de preparación física para las clases de tenis, es la clase de preparación y durabilidad de estado físico.\r\nGracias al apoyo de PrimerGolpe.com', '', '11 de January 2012', '2012-01-11', '', 3, 0);
INSERT INTO `noticias` VALUES (28, '', 'tuto N 4', 'espero que ahora si funcione porque mellaman para almorzar ja ... esta la prueba 3 ', '<iframe width="380" height="300" src="http://www.youtube.com/embed/8WBN1vVTdSs" frameborder="0" allowfullscreen></iframe>', '22 de February 2012', '2012-02-22', '', 1, 0);
