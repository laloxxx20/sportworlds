<?
include_once "class/classBD/classNoticiaBD.php";
include_once "class/classBD/classComentarioBD.php";
include_once "class/classBD/classCategoriaBD.php";// yet useable ... changing to MVC with de newt link
include_once "class/classController/classSystem.php";

$url="http://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
$urlLugar= dirname($url);

/*********************(S)give data from video with system.php***************/
$dataVideo=new System();// object that connects the controlles with tha part visual
$categorias=$dataVideo->showCategorys(); // just to menu
$verr=$dataVideo->showDataVideo($_GET['inputSearch'],$_GET['no']); // data of all video
/*********************(E)give data from video with system.php***************/
        
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>
            <?
               echo $verr["categoria"]." : ".$verr['title'];// titulo del video con su categoria                                  
            ?>
        </title>
        <script language="javascript" type="text/javascript" src="js/ajax/ajaxToComments.js"></script>
        <script language="javascript" type="text/javascript" src="js/ajax/ajaxToFormComments.js"></script>
        <script language="javascript" type="text/javascript" src="js/funciones.js"></script>
        <script language="javascript" type="text/javascript" src="js/contenedorCentro.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>

        <script src="js/jquery.js"></script>
        <link href="css/estilos.css" type="text/css" rel="stylesheet" />
        <link href="css/estilosDesarrolloNoticia.css" type="text/css" rel="stylesheet" />
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"> 
        <? include_once "css/cssConPhp.php"; ?>

    </head>

    <body onload="llamarAjaxParaComents(<? echo $_GET['no']; ?> , 1, 7),posicionDivFormulario()"> 
                  <!-- aqui se cargan los primeros comentarios-->    

    <center>
        <div id="principal"><!--se cierra en ContenedorLastHeader-->

            <div id="header">

                <div id="headerIzquierda">
                    <div id="bienvenida">
                        <div id="cajaBienvenida">
                            <h2>Bienvenido a SportsWorld</h2>
                            <!--<a href="cerrar.php">Cerrar Sesi&oacute;n</a>-->
                        </div>
                    </div>
                </div>

                <div id="headerCentro">
                    <div id="contenedorImagen">
                            <img src="imagenes/sportWorldLogo.png" alt="imagen" width="100"  />
                    </div>    
                </div>

                <div id="headerDerecha">
                </div>
            </div>

            <?        
                include_once "answerContent/answerContentL.php";   
                include_once "indexContenido/ContenedorBodyCentro.php";        
                include_once "answerContent/answerContentR.php";                
            ?>                

            <div id="lastHeader" >
                <hr>
                    &copy; Desarrollado por Eduardo Chauca - <? echo date("Y");?>
                <hr>        
            </div>
        </div> <!--se esta abriendo en ContenedorHeader-->   
    </center>  

    </body>
</html>